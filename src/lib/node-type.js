const ACTION = 'action'
const CONDITION = 'condition'
const VARIABLE = 'variable'

export { ACTION, CONDITION, VARIABLE }