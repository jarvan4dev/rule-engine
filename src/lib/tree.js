import Node from './node'

import * as NodeType from '@/lib/node-type'

export default class Tree {
    constructor(meta = null) {
        this._root = new Node(NodeType.VARIABLE, meta)
    }
    findById(id) {
        let queue = []
        queue.push(this._root)
        let currentNode = queue.shift()
        while(currentNode) {
            if (currentNode.meta.id === id) {
                return currentNode
            }
            queue.push(...currentNode.children)
            currentNode = queue.shift()
        }
    }
    nodes() {
        return this._root.nodes()
    }
    coordinates() {
        let coordinates = [], queue = []
        queue.push(this._root)
        let currentNode = queue.shift()
        while (currentNode) {
            currentNode.children.forEach(child => {
                coordinates.push({
                    startX: currentNode.meta.x + currentNode.meta.width * 0.8,
                    startY: currentNode.meta.y + 16,
                    endX: child.meta.x,
                    endY: child.meta.y + 16
                })
                queue.push(child)
            })
            currentNode = queue.shift()
        }
        return coordinates
    }
    height() {
        let nodes = this.nodes()
        let height = 0
        nodes.forEach(node => {
            if (node.meta.y > height ) {
                height = node.meta.y
            }
        })
        return height + 50
    }
    width() {
        let nodes = this.nodes()
        let width = 0
        nodes.forEach(node => {
            if (node.meta.x > width ) {
                width = node.meta.x
            }
        })
        return width + 180
    }
}