import * as NodeType from './node-type'
import Node from './node'

export function findLastChildNode(currentNode) {
    if (currentNode.children.length !== 0) {
        let lastNode = currentNode.children[currentNode.children.length - 1]
        let nextLastNode = findLastChildNode(lastNode)
        if (nextLastNode) {
            return nextLastNode
        }
        return lastNode
    }
}

export function findFirstChildNode(currentNode) {
    if (currentNode.children.length !== 0) {
        let firstNode = currentNode.children[0]
        let nextFirstNode = findFirstChildNode(firstNode)
        if (nextFirstNode) {
            return nextFirstNode
        }
        return firstNode
    }
}

export function resetItemPosition(node) {
    let parentNode = node.parent
    if (parentNode) {
        let nodes = parentNode.children
        let index = nodes.indexOf(node)
        for (let i = index + 1; i < nodes.length; i++) {
            // nodes()方法包含了节点本身
            let children = nodes[i].nodes()
            for (let j = 0; j < children.length; j++) {
                children[j].meta.y += 50
            }
        }
        resetItemPosition(parentNode)
    }
}

export function resetChildrenX(node) {
    let children = node.children
    children.forEach(child => {
        child.meta.x = node.getAllWidth() + 40
        resetChildrenX(child)
    })
}

export function resetParentPosition(node) {
    let parentNode = node.parent
    if (parentNode) {
        let childrenCount = parentNode.children.length
        if (childrenCount > 0) {
            let firstChild = parentNode.children[0]
            let lastChild = parentNode.children[parentNode.children.length - 1]
            parentNode.meta.y = (firstChild.meta.y + lastChild.meta.y) / 2
            resetParentPosition(parentNode)
        }
    }
}

export function serialize(node, result) {
    let data = node.data
    let nodeType = node.type
    if (nodeType === NodeType.VARIABLE) {
        result.variableLeft = data
    }
    if (nodeType === NodeType.CONDITION) {
        result.op = data.op
        result.param = data.param
    }
    let nodes = []
    node.children.forEach(child => {
        if(child.type === NodeType.ACTION) {
            let templateParams = child.data
            let templateAction = {templateParams}
            result.actionNode = {templateAction}
        } else {
            let ruleTreeConditionNode = {}
            serialize(child, ruleTreeConditionNode)
            if (!_.isEmpty(ruleTreeConditionNode)) {
                nodes.push(ruleTreeConditionNode)
            }
        }
    })
    if (nodes.length > 0) {
        let firstNode = nodes[0]
        if (_.has(firstNode, 'variableLeft')) {
            result.ruleTreeVariableNodes = nodes
        } else {
            result.ruleTreeConditionNodes = nodes
        }
    }
}

function deserializeNodes(node, data) {
    data.forEach(ruleTreeNode => {
        let op = ruleTreeNode['op']
        let param = ruleTreeNode['param']
        let variableLeft = ruleTreeNode['variableLeft']

        let child
        if (variableLeft) {
            child = new Node(NodeType.VARIABLE, null, variableLeft)
        } else {
            child = new Node(NodeType.CONDITION, null, {op, param})
        }
        child.parent = node
        let x = node.getAllWidth() + 40
        let y = node.meta.y
        // 新加入节点的兄弟节点
        let brothers = node.children
        if (brothers.length > 0) {
            y = brothers[brothers.length - 1].meta.y + 50
            resetItemPosition(node)
        }
        child.meta = {x: x, y: y}
        node.addChild(child)

        resetParentPosition(child)

        // 处理当前子节点的孩子
        let actionNode = ruleTreeNode['actionNode']
        let ruleTreeVariableNodes = ruleTreeNode['ruleTreeVariableNodes']
        let ruleTreeConditionNodes = ruleTreeNode['ruleTreeConditionNodes']
        if (actionNode) {
            let grandChild = new Node(NodeType.ACTION, null, actionNode['templateAction']['templateParams'])
            grandChild.parent = child
            x = child.getAllWidth() + 40
            y = child.meta.y
            grandChild.meta = {x: x, y: y}
            child.addChild(grandChild)
        }
        if (ruleTreeVariableNodes) {
            deserializeNodes(child, ruleTreeVariableNodes)
        }
        if (ruleTreeConditionNodes) {
            deserializeNodes(child, ruleTreeConditionNodes)
        }
    })
}

export function deserialize(data, node) {
    _.keys(data).forEach(key => {
        let value = data[key]
        if (key === 'variableLeft') {
            // 条件节点
            node.type = NodeType.VARIABLE
            node.setData(value)
            node.meta = {x: 0, y: 0}
        } 
        if (key === 'ruleTreeConditionNodes') {
            deserializeNodes(node, value)
        }
    })
}