import Vue from 'vue'
import iView from 'iview'
import 'iview/dist/styles/iview.css'
import ContentMenu from 'v-contextmenu'
import 'v-contextmenu/dist/index.css'
import App from './App.vue'
import router from './router'
import store from './store'

import Mock from './mock'

Vue.use(iView)
Vue.use(ContentMenu)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
