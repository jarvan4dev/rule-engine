// 引入mockjs
import Mock from 'mockjs'
import _ from 'lodash'
// 获取 mock.Random 对象
const Random = Mock.Random;

const operatorData = [{
    "opTypeEnName": "Number_Equals",
    "opTypeCnName": "等于"
}, {
    "opTypeEnName": "Number_NotEquals",
    "opTypeCnName": "不等于"
}, {
    "opTypeEnName": "Number_LessThen",
    "opTypeCnName": "小于"
}, {
    "opTypeEnName": "Number_LessThenEquals",
    "opTypeCnName": "小于等于"
}, {
    "opTypeEnName": "Number_GreaterThen",
    "opTypeCnName": "大于"
}, {
    "opTypeEnName": "Number_GreaterThenEquals",
    "opTypeCnName": "大于等于"
}, {
    "opTypeEnName": "Number_In",
    "opTypeCnName": "在集合中"
}, {
    "opTypeEnName": "Number_NotIn",
    "opTypeCnName": "不在集合中"
}, {
    "opTypeEnName": "Number_Range",
    "opTypeCnName": "在范围内"
}]

const operators = {
    "code": 0,
    "message": "success",
}

const factors = {
    "code": 0,
    "message": "success",
    "data": [{
            "variableName": "is_renewal",
            "variableLabel": "是否续保",
            "variableCategory": "risk_property",
            "variableCategoryLabel": "跟单属性",
            "variableType": "Boolean"
        }, {
            "variableName": "claim_count",
            "variableLabel": "商业险上年赔款次数",
            "variableCategory": "risk_property",
            "variableCategoryLabel": "跟单属性",
            "variableType": "Integer"
    }]
}

const actions = {
    "code": 0,
    "message": "success",
    "data": {
        "assertAction": null,
        "templateAction": {
            "actionName": "参数模板配置",
            "templateParams": [
                {
                    "variableName": "biz",
                    "variableLabel": "商业险返点",
                    "variableType": "Double",
                    "variableValue": "0"
                },
                {
                    "variableName": "force",
                    "variableLabel": "交强险返点",
                    "variableType": "Double",
                    "variableValue": "0"
                }
            ]
        }
    }
}

const ruleSet = {"variableLeft":{"variableName":"is_renewal","variableLabel":"是否续保","variableCategory":"risk_property","variableCategoryLabel":"跟单属性","variableType":"Boolean"},"ruleTreeConditionNodes":[{"op":"Number_Equals","param":"111","actionNode":{"templateAction":{"templateParams":[{"variableName":"biz","variableLabel":"商业险返点","variableType":"Double","variableValue":"0"},{"variableName":"force","variableLabel":"交强险返点","variableType":"Double","variableValue":"0"}]}}},{"op":"Number_GreaterThen","param":"2222","ruleTreeVariableNodes":[{"variableLeft":{"variableName":"is_renewal","variableLabel":"是否续保","variableCategory":"risk_property","variableCategoryLabel":"跟单属性","variableType":"Boolean"},"ruleTreeConditionNodes":[{"op":"Number_LessThen","param":"22222","actionNode":{"templateAction":{"templateParams":[{"variableName":"biz","variableLabel":"商业险返点","variableType":"Double","variableValue":"0"},{"variableName":"force","variableLabel":"交强险返点","variableType":"Double","variableValue":"0"}]}}}]}]},{"op":"Number_LessThenEquals","param":"而我却二无群","ruleTreeVariableNodes":[{"variableLeft":{"variableName":"claim_count","variableLabel":"商业险上年赔款次数","variableCategory":"risk_property","variableCategoryLabel":"跟单属性","variableType":"Integer"},"ruleTreeConditionNodes":[{"op":"Number_NotEquals","param":"111","actionNode":{"templateAction":{"templateParams":[{"variableName":"biz","variableLabel":"商业险返点","variableType":"Double","variableValue":"0"},{"variableName":"force","variableLabel":"交强险返点","variableType":"Double","variableValue":"0"}]}}},{"op":"Number_Equals","param":"111","actionNode":{"templateAction":{"templateParams":[{"variableName":"biz","variableLabel":"商业险返点","variableType":"Double","variableValue":"0"},{"variableName":"force","variableLabel":"交强险返点","variableType":"Double","variableValue":"0"}]}}},{"op":"Number_Equals","param":"111","actionNode":{"templateAction":{"templateParams":[{"variableName":"biz","variableLabel":"商业险返点","variableType":"Double","variableValue":"0"},{"variableName":"force","variableLabel":"交强险返点","variableType":"Double","variableValue":"0"}]}}},{"op":"Number_Equals","param":"1111","ruleTreeVariableNodes":[{"variableLeft":{"variableName":"claim_count","variableLabel":"商业险上年赔款次数","variableCategory":"risk_property","variableCategoryLabel":"跟单属性","variableType":"Integer"},"ruleTreeConditionNodes":[{"op":"Number_Equals","param":"111","actionNode":{"templateAction":{"templateParams":[{"variableName":"biz","variableLabel":"商业险返点","variableType":"Double","variableValue":"0"},{"variableName":"force","variableLabel":"交强险返点","variableType":"Double","variableValue":"0"}]}}},{"op":"Number_NotEquals","param":"111","actionNode":{"templateAction":{"templateParams":[{"variableName":"biz","variableLabel":"商业险返点","variableType":"Double","variableValue":"0"},{"variableName":"force","variableLabel":"交强险返点","variableType":"Double","variableValue":"0"}]}}}]}]}]}]}]}

Mock.mock(/\/meta\/factors(\?|$)/, factors);

Mock.mock(/\/meta\/operators(\?|$)/, function () {
    operators.data = operatorData.slice(0, _.random(4, 6))
    return operators
});

Mock.mock(/\/meta\/actions(\?|$)/, actions);

Mock.mock(/\/manage\/query(\?|$)/, {
    code: 0,
    message: 'success',
    data: ruleSet
});
