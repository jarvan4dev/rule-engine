import Vue from 'vue'
import Vuex from 'vuex'

import App from './app'
import Meta from './meta'
import Walk from './walk'
import Manage from './manage'

Vue.use(Vuex)

const store = new Vuex.Store({
    modules: {
        App,
        Meta,
        Walk,
        Manage
    }
})

export default store