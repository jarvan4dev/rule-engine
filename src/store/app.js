import Node from '@/lib/node'
import Tree from '@/lib/tree'

import {findLastChildNode, resetParentPosition, resetItemPosition, resetChildrenX, serialize} from "../lib/node-utils"

const app = {
    state: {
        tree: undefined,
        currentNode: undefined
    },
    mutations: {
        SET_TREE: (state, tree) => {
            state.tree = tree
        },
        ADD_NODE: (state, nextNode) => {
            state.currentNode.addChild(nextNode)
        },
        SET_CURRENT_NODE: (state, node) => {
            state.currentNode = node
        }
    },
    actions: {
        // 重新计算当前节点的所有子元素X轴的位置
        ReCalculateChildrenX({ state }) {
            resetChildrenX(state.currentNode)
        },
        AddNode({ dispatch, commit, state }, type) {
            let id = state.tree._root.maxId() + 1
            let x = state.currentNode.getAllWidth()
            let y = state.currentNode.meta.y
            let lastNode = findLastChildNode(state.currentNode)
            if (lastNode) {
                // 50 代表节点直接的间隔
                y = lastNode.meta.y + 50
            }
            let nextNodeParam = {id: id, x: x + 40, y: y}
            let node = new Node(type, nextNodeParam)
            commit('ADD_NODE', node)
            if (state.currentNode.children.length > 1) {
                resetItemPosition(state.currentNode)
            }
            resetParentPosition(node)
        },
        SetCurrentNode({ commit, state }, id) {
            let node = state.tree.findById(id)
            commit('SET_CURRENT_NODE', node)
        },
        RemoveNode({commit, state}, node) {
            let parent = node.parent
            if (!parent) {
                return
            }
            let index = parent.children.indexOf(node)
            let length = parent.children.length
            parent.removeChild(node)
            // 重置位置
            if (index !== length - 1) {
                let height = node.getAllHeight()
                let children = parent.children
                for (let i = index; i < children.length; i++) {
                    children[i].nodes().forEach(child => {
                        child.meta.y -= height
                    })
                }
            }
            resetParentPosition(node)

            // 必须要加一个这个，否则 state不会变化
            commit('SET_TREE', state.tree)
        },
        SetNodeData({commit, state}, {node, data}) {
            node.setData(data)
            // 必须要加一个这个，否则 state不会变化
            commit('SET_TREE', state.tree)
        },
        DoSubmit({state}) {
            let result = {}
            serialize(state.tree._root, result)
            console.log(JSON.stringify(result))
        }
    },
    getters: {
        nodeList: state => {
            if (state.tree) {
                return state.tree._root.nodes()
            }
        },
        coordinates: state => {
            if (state.tree) {
                return state.tree.coordinates()
            }
        },
        height: state => {
            if (state.tree) {
                return state.tree.height()
            }
        },
        width: state => {
            if (state.tree) {
                return state.tree.width()
            }
        }
    }
}
export default app