import { fetchFactors, fetchOperators, fetchActions } from '@/api/meta'

const meta = {
    state: {
        factors: [],
        operators: [],
        actions: []
    },
    mutations: {
        SET_FACTORS(state, factors) {
            state.factors = factors
        },
        SET_OPERATORS(state, operators) {
            state.operators = operators
        },
        SET_ACTIONS(state, actions) {
            state.actions = actions
        }
    },
    actions: {
        async FetchFactors({commit}, params) {
            let response = await fetchFactors(params)
            let { code, message, data } = response.data
            if (code !== 0) {
                throw new Error(message)
            }
            commit('SET_FACTORS', data)
        },
        async FetchOperators({commit}, params) {
            let response = await fetchOperators(params)
            let { code, message, data } = response.data
            if (code !== 0) {
                throw new Error(message)
            }
            commit('SET_OPERATORS', data)
        },
        async FetchActions({commit}, params) {
            let response = await fetchActions(params)
            let { code, message, data } = response.data
            if (code !== 0) {
                throw new Error(message)
            }
            commit('SET_ACTIONS', data.templateAction.templateParams)
        }
    },
    getters: {
        factors: state => state.factors,
        operators: state => state.operators,
        actions: state => state.actions
    }
}

export default meta