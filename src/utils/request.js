import axios from 'axios'

const service = axios.create({
    // baseURL: 'http://172.17.0.171:8055',
    baseURL: '',
    timeout: 5000
})

service.interceptors.response.use(response => {
    // console.log(response)
    return response
}, error => Promise.reject(error.response.data))

export default service
