import request from '@/utils/request'

export function fetchFactors(params) {
    return request({
        url: '/meta/factors',
        params: params
    })
}

export function fetchOperators(params) {
    return request({
        url: '/meta/operators',
        params: params
    })
}

export function fetchActions(params) {
    return request({
        url: '/meta/actions',
        params: params
    })
}