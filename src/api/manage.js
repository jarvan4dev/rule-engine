import request from '@/utils/request'

export function fetchRuleSet(params) {
    return request({
        url: '/manage/query',
        params: params
    })
}

export function addRuleSet(params) {
    return request({
        url: '/manage/add',
        method: 'post',
        data: params
    })
}

export function updateRuleSet(params) {
    return request({
        url: '/manage/update',
        method: 'post',
        data: params
    })
}

export function deleteRuleSet(params) {
    return request({
        url: '/manage/delete',
        method: 'post',
        data: params
    })
}
