import request from '@/utils/request'

export function fetchRule(params) {
    return request({
        url: '/walk/walk-rule',
        method: 'post',
        data: params
    })
}
