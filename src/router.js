import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [{
      path: '/rule',
      name: 'rule',
      component: () => import('./views/Rule.vue')
    }, {
      path: '/list',
      name: 'list',
      component: () => import('./views/List.vue')
  }]
})
